﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week3ex
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = new string[5] { "red", "blue", "orange", "white", "black" };

            for (var i = 0; i < colours.Length; i++)
            {
                Console.WriteLine(colours[i]);
            }
        }
    }
}
